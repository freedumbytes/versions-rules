pipeline
{
  agent
  {
    label "windows"
  }



  environment
  {
    MAVEN_SHOW_TIME = "-Dorg.slf4j.simpleLogger.showDateTime=true"
    MAVEN_FORMAT_TIME = "-Dorg.slf4j.simpleLogger.dateTimeFormat=EEE..yyyy-MM-dd...HH:mm:ss.SSS..z"
    MAVEN_SUPPRESS_DOWNLOAD_LOGS = "-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN"
    MAVEN_PREVENT_RECOMPILE = "-Dmaven.compiler.useIncrementalCompilation=false"
    MAVEN_OPTS = "-Djava.awt.headless=true ${MAVEN_SHOW_TIME} ${MAVEN_FORMAT_TIME} ${MAVEN_SUPPRESS_DOWNLOAD_LOGS}"
    MAVEN_CLI_OPTS = "--batch-mode --errors --fail-fast --show-version ${MAVEN_PREVENT_RECOMPILE}"

    COMMIT_HASH = sh(returnStdout: true, script: "git log -1 --pretty=format:'%H'").trim()
    COMMIT_HASH_SHORT = sh(returnStdout: true, script: "git log -1 --pretty=format:'%h'").trim()
    COMMIT_MESSAGE = sh(returnStdout: true, script: "git log -1 --pretty=format:'%B'").trim()
    COMMIT_COMMITER = sh(returnStdout: true, script: "git log -1 --pretty=format:'%an'").trim()
  }



  stages
  {
    stage("build")
    {
      steps
      {
        mvnCompileNode nodeName: "windows", profile: "", branchNames: []
      }
    }

    stage("test")
    {
      steps
      {
        parallel (
          "unit":
          {
            mvnUnitTestNode nodeName: "windows", profile: "", branchNames: []
          },

          "integration-artifacts":
          {
            mvnIntegrationTestNode nodeName: "windows", profile: "-Pdocuments,integrationTestsOnly", branchNames: ['master']
          },
        )
      }

      post
      {
        always
        {
          jenkinsTestReportNode nodeName: "windows", profile: "", branchNames: []
        }
      }
    }

    stage("deploy")
    {
      steps
      {
        parallel (
          "quality-assurance":
          {
            mvnSonarQubeNode nodeName: "windows", profile: "", branchNames: ['master']
          },

          "documentation":
          {
            mvnSiteNode nodeName: "windows", profile: "-PenableUpdatesReports,enableJavadocReports", branchNames: ['master']
          }
        )
      }
    }

    stage("open-source")
    {
      steps
      {
        parallel (
          "quality-assurance":
          {
            mvnSonarQubeNode nodeName: "windows", profile: "-PopenSource,sonarCloud", branchNames: ['master']
          },

          "artifacts":
          {
            mvnOpenSourceReleaseNode nodeName: "windows", profile: "-DskipTests -PopenSource,documents", branchNames: ['master']
          }
        )
      }
    }
  }

  post
  {
    failure
    {
      mvnEffectivePom nodeName: "windows", profile: "", branchNames: []
    }
  }
}





def mvnCompileNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("compile")
    {
      echo "Check Out..."
      checkout scm

      echo "Clean Up..."
      sh "git config --system core.longpaths true"
      sh "git clean -fdx"

      echo "Commit Hash: ${env.COMMIT_HASH}"
      echo "Commit Hash Short: ${env.COMMIT_HASH_SHORT}"
      echo "Commit Message: '${env.COMMIT_MESSAGE}'"
      echo "Committer: ${env.COMMIT_COMMITER}"

      if (hasReleaseTag())
      {
        echo "Commit Tags: '${env.COMMIT_TAGS}'"
      }

      echo "Compile Source..."
      sh "mvn $MAVEN_CLI_OPTS test-compile ${args.profile}"
      stash name: "bytecode", includes: "**/target/**,**/generated/**", allowEmpty: true
    }
  }
}



def mvnUnitTestNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("unit")
    {
      echo "Check Out..."
      checkout scm

      echo "Clean Up..."
      sh "git config --system core.longpaths true"
      sh "git clean -fdx"

      echo "Bytecode..."
      unstash "bytecode"
      sh "find . -name \"*.class\" -exec touch {} \\+"

      try
      {
        echo "Unit Test..."
        sh "mvn $MAVEN_CLI_OPTS test ${args.profile}"
      }
      finally
      {
        stash name: "unittests", includes: "**/target/surefire-reports/*,**/target/coverage-reports/*", allowEmpty: true
      }
    }
  }
}



def mvnIntegrationTestNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("integration-artifacts")
    {
      echo "Check Out..."
      checkout scm

      echo "Clean Up..."
      sh "git config --system core.longpaths true"
      sh "git clean -fdx"

      echo "Bytecode..."
      unstash "bytecode"
      sh "find . -name \"*.class\" -exec touch {} \\+"

      try
      {
        echo "Integration Test..."

        if (args.branchNames.contains(env.BRANCH_NAME) || hasReleaseTag())
        {
          sh "mvn $MAVEN_CLI_OPTS deploy ${args.profile}"
        }
        else
        {
          sh "mvn $MAVEN_CLI_OPTS install ${args.profile}"
        }

        sh "mvn $MAVEN_CLI_OPTS animal-sniffer:check ${args.profile}"
        sh "mvn $MAVEN_CLI_OPTS enforcer:enforce ${args.profile}"
      }
      finally
      {
        stash name: "integrationtests", includes: "**/target/failsafe-reports/*,**/target/coverage-reports/*", allowEmpty: true
      }
    }
  }
}



def mvnOpenSourceReleaseNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("release-artifacts")
    {
      if (hasReleaseTag())
      {
        echo "Check Out..."
        checkout scm

        echo "Clean Up..."
        sh "git config --system core.longpaths true"
        sh "git clean -fdx"

        echo "Bytecode..."
        unstash "bytecode"
        sh "find . -name \"*.class\" -exec touch {} \\+"

        echo "Open Source Release for '${env.COMMIT_TAGS}'"

        sh "mvn $MAVEN_CLI_OPTS deploy ${args.profile}"
      }
      else
      {
        echo "Skip Open Source SNAPSHOT for '${env.COMMIT_MESSAGE}'."
      }
    }
  }
}



def jenkinsTestReportNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("test-report")
    {
      echo "Clean Up..."
      deleteDir()

      echo "Testresults..."
      unstash "unittests"
      unstash "integrationtests"

      junit testResults: "**/target/*-reports/*.xml", allowEmptyResults: true
    }
  }
}



def mvnSonarQubeNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("sonarqube")
    {
      if (args.branchNames.contains(env.BRANCH_NAME) || hasReleaseTag())
      {
        echo "Check Out..."
        checkout scm

        echo "Clean Up..."
        sh "git config --system core.longpaths true"
        sh "git clean -fdx"

        echo "Bytecode..."
        unstash "bytecode"
        sh "find . -name \"*.class\" -exec touch {} \\+"

        echo "Testresults..."
        unstash "unittests"
        unstash "integrationtests"

        echo "Dependency Check..."
        sh "mvn $MAVEN_CLI_OPTS org.owasp:dependency-check-maven:aggregate ${args.profile}"

        echo "Quality Assurance..."
        sh "mvn $MAVEN_CLI_OPTS sonar:sonar ${args.profile}"
      }
      else
      {
        echo "Skip SonarQube for branch '${env.BRANCH_NAME}'."
      }
    }
  }
}



def mvnSiteNode(Map args)
{
  node("${args.nodeName}")
  {
    stage("site")
    {
      if (args.branchNames.contains(env.BRANCH_NAME))
      {
        echo "Check Out..."
        checkout scm

        echo "Clean Up..."
        sh "git config --system core.longpaths true"
        sh "git clean -fdx"

        echo "Bytecode..."
        unstash "bytecode"
        sh "find . -name \"*.class\" -exec touch {} \\+"

        echo "Testresults..."
        unstash "unittests"
        unstash "integrationtests"

        echo "Site Deployment..."
        sh "mvn $MAVEN_CLI_OPTS site-deploy ${args.profile}"
      }
      else
      {
        echo "Skip Site Deployment for branch '${env.BRANCH_NAME}'."
      }
    }
  }
}



def mvnEffectivePom(Map args)
{
  node("${args.nodeName}")
  {
    stage("pom")
    {
      echo "Check Out..."
      checkout scm

      echo "Clean Up..."
      sh "git config --system core.longpaths true"
      sh "git clean -fdx"

      echo "Effective POM..."
      sh "mvn $MAVEN_CLI_OPTS help:effective-pom ${args.profile}"
    }
  }
}



def boolean hasReleaseTag() {
  if (!env.COMMIT_TAGS)
  {
    sh "git fetch --tags"
    env.COMMIT_TAGS = sh(returnStdout: true, script: "git show-ref --tags -d | grep '^${COMMIT_HASH_SHORT}' | sed -e 's,.* refs/tags/,,' -e 's/\\^{}//'").trim()
  }

  return (env.COMMIT_TAGS != "" && env.COMMIT_MESSAGE.contains("[maven-release-plugin] prepare release"))
}
