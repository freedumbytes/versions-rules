
[comment]: # (Project specific information)

# Maven Versions Rules

[![Maven Versions Rules License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "Maven Versions Rules License")](https://www.apache.org/licenses/LICENSE-2.0.html)
[![Maven Versions Rules pipeline](https://gitlab.com/freedumbytes/versions-rules/badges/master/build.svg "Maven Versions Rules pipeline")](https://gitlab.com/freedumbytes/versions-rules)

[![Maven Versions Rules Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Versions Rules Maven Site")](https://freedumbytes.gitlab.io/versions-rules/)
[![Maven Versions Rules Maven Site](https://javadoc.io/badge/nl.demon.shadowland.freedumbytes.maven.versioning/versions-rules.svg?color=yellow&label=Maven%20Versions%20Rules "Maven Versions Rules Maven Site")](https://freedumbytes.gitlab.io/versions-rules/)

[![Maven Versions Rules Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Maven Versions Rules Maven Central")](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.maven.versioning%22)
[![Maven Versions Rules Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.maven.versioning/versions-rules.svg?label=Maven%20Central "Maven Versions Rules Maven Central")](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.maven.versioning%22)

[![Maven Versions Rules Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Maven Versions Rules Nexus")](https://oss.sonatype.org/index.html#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.versioning~~~~)
[![Maven Versions Rules Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.versioning/versions-rules.svg?label=Nexus "Maven Versions Rules Nexus")](https://oss.sonatype.org/index.html#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.versioning~~~~)

[![Maven Versions Rules MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Maven Versions Rules MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.versioning)
[![Maven Versions Rules MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.versioning/versions-rules.svg?label=MvnRepository "Maven Versions Rules MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.versioning)

[![Maven Versions Rules SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Maven Versions Rules SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules)
[![Maven Versions Rules Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=alert_status "Maven Versions Rules Quality Gate")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules)
[![Maven Versions Rules vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=vulnerabilities "Maven Versions Rules vulnerabilities")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=vulnerabilities)
[![Maven Versions Rules bugs](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=bugs "Maven Versions Rules bugs")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=bugs)

[![Maven Versions Rules SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Maven Versions Rules SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules)
[![Maven Versions Rules lines of code](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=ncloc "Maven Versions Rules lines of code")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=ncloc)
[![Maven Versions Rules duplication](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=duplicated_lines_density "Maven Versions Rules duplication")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=duplicated_lines)
[![Maven Versions Rules technical debt](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=sqale_index "Maven Versions Rules technical debt")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.versioning:versions-rules&metric=sqale_index)

[![Maven Versions Rules Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Maven Versions Rules Dependency Check")](https://freedumbytes.gitlab.io/versions-rules/dependency-check-report.html)
[![Maven Versions Rules Dependency Check](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.versioning/versions-rules.svg?label=Dependency%20Check "Maven Versions Rules Dependency Check")](https://freedumbytes.gitlab.io/versions-rules/dependency-check-report.html)

[comment]: # (Project specific information)

#### Legend

  * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Maven Site](https://maven.apache.org/plugins/maven-site-plugin/) generated site also includes the project's reports that were configured in the POM.
  * [![Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Central") The Central Repository](https://search.maven.org/).
  * [![Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Nexus") Nexus Repository Manager](https://oss.sonatype.org/index.html).
  * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") MvnRepository](https://mvnrepository.com/) is like Google for Maven artifacts with Dependencies Updates information.
  * [![SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "SonarCloud") SonarCloud](https://sonarcloud.io/explore/projects?sort=-analysis_date) is a widely adopted open source platform to inspect continuously the quality of source code and detect bugs, vulnerabilities and code smells in more than 20 different languages.
  * [![Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Dependency Check") Dependency Check](https://www.owasp.org/index.php/OWASP_Dependency_Check) is a utility that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities.

# Available Documentation

Overall project documentation can be found at:

 * [![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") freedumbytes.bitbucket.io](https://freedumbytes.bitbucket.io/index.xhtml)
 * [![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") freedumbytes.github.io](https://freedumbytes.github.io/index.xhtml)
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") freedumbytes.gitlab.io](https://freedumbytes.gitlab.io/index.xhtml)

Those sites can also be found through my Home Page [![Shadowland](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "Shadowland") www.shadowland.demon.nl](http://www.shadowland.demon.nl/index.xhtml).

# Available Projects

Currently using the following git repositories:

 * [![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") Bitbucket](https://bitbucket.org/freedumbytes/)
   ([System Metrics](https://status.bitbucket.org) or [![Bitbucket Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "Bitbucket Twitter") @bitbucketstatus](https://twitter.com/bitbucketstatus))
 * [![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") GitHub](https://github.com/freedumbytes/)
   ([Status Messages](https://status.github.com) or [![GitHub Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "GitHub Twitter") @githubstatus](https://twitter.com/githubstatus))
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") GitLab](https://gitlab.com/freedumbytes/)
   ([System Metrics](https://status.gitlab.com) or [![GitLab Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "GitLab Twitter") @gitlabstatus](https://twitter.com/gitlabstatus))

Check the following [git command examples](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#vcs.git.commands).

# Development Production Line - The Short Story

The [Development Production Line](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html) manual (as of version `4.0-beta`) contains a description how the following projects came about:

 1. [Maven Setup](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.setup) the Maven site, license and plugin configuration in a parent `POM`.

 2. [Maven Versions Rules](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.versions.rules) the Maven `ruleSet` file containing the rules that control how to compare version numbers.

    **Note**: The `rules.xml` was almost removed with version `3.0.0` of `versions-maven-plugin` (see also [issue 157](https://github.com/mojohaus/versions-maven-plugin/issues/157)).

 3. [Custom Dependency Check](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.dependency.check.custom.suppression.hints) `suppressionFile` and `hintsFile` containing the rules that control how to suppress false positives and resolve false negatives.

 4. [Bill Of Materials](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.grouping.dependencies) to setup the required versions of the most popular Maven dependencies and group them logically together.

 5. [Custom Maven Skins](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.site.skin.custom) configuration.

 6. [JModalWindow](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) was created to support modal functionality, similar to `JDialog`, without blocking all frames. And was migrated from Ant to Maven to demonstrate `maven.compiler.target` usage.

 7. [Basketball](https://gitlab.com/freedumbytes/basketball/) Data Storage of Game Statistics.

#### 1. Maven Setup

To use the site, license and plugin configuration of [Maven Setup](https://gitlab.com/freedumbytes/setup):

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven</groupId>
    <artifactId>setup</artifactId>
    <version>x.y.z</version>
  </parent>
```

To also use the Java build and reporting settings:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven</groupId>
    <artifactId>java</artifactId>
    <version>x.y.z</version>
  </parent>
```

Plus the following subset of the `setup` configured properties:

```xml
  <properties>
    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.target}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>
    <!--
      Requires Java9+, otherwise: 'mvn compile' results in "javac: invalid flag: --release".
      <maven.compiler.release>8</maven.compiler.release>
    -->

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>
    <extraEnforcerRulesFail>true</extraEnforcerRulesFail>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>
  </properties>
```

Those properties can be changed when:

  * Compiling for older JDKs.
  * Compiling and testing with different source levels.
  * Single module projects should use `aggregateSurefireReports` `false`, otherwise the Surefire report will always show that the number of tests is `0` and the Failsafe report will be missing altogether.
  * Error: Could not find or load main class org.apache.maven.surefire.booter.ForkedBooter (see also [Class Loading and Forking in Maven Surefire](https://maven.apache.org/surefire/maven-surefire-plugin/examples/class-loading.html)).


#### 2. Maven Versions Rules

To use the ruleSet file containing the [Versions Rules](https://gitlab.com/freedumbytes/versions-rules) that control how to compare version numbers:

```xml
  <properties>
    <ossrhHost>https://oss.sonatype.org</ossrhHost>
    <nexusHost>${ossrhHost}</nexusHost>

    <versionsRulesVersion>x.y.z</versionsRulesVersion>
    <versionsRulesFolderVersion>${versionsRulesVersion}</versionsRulesFolderVersion>
    <versionsRulesPath>
      ${nexusHost}/content/groups/public/nl/demon/shadowland/freedumbytes/maven/versioning/versions-rules/${versionsRulesFolderVersion}
    </versionsRulesPath>
  </properties>

  <profiles>
    <profile>
      <id>enableUpdatesReports</id>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>versions-maven-plugin</artifactId>
            <reportSets>
              <reportSet>
                <reports>
                  <report>dependency-updates-report</report>
                  <report>plugin-updates-report</report>
                  <report>property-updates-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${versionsMavenPluginVersion}</version>
          <configuration>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
            <rulesUri>${versionsRulesPath}/versions-rules-${versionsRulesVersion}.xml</rulesUri>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

#### 3. Custom Dependency Check

To use the [Custom Dependency Check](https://gitlab.com/freedumbytes/dependency-check) `suppressionFile` and `hintsFile` containing the rules that control how to suppress false positives and resolve false negatives:

```xml
  <properties>
    <ossrhHost>https://oss.sonatype.org</ossrhHost>
    <nexusHost>${ossrhHost}</nexusHost>

    <owaspReportsPath>${project.build.directory}/owasp-reports</owaspReportsPath>
    <owaspReportFormat>all</owaspReportFormat>
    <cveValidForHours>24</cveValidForHours>

    <customSuppressionHintsVersion>x.y.z</customSuppressionHintsVersion>
    <customSuppressionHintsFolderVersion>${customSuppressionHintsVersion}</customSuppressionHintsFolderVersion>
    <customSuppressionHintsPath>
      ${nexusHost}/content/groups/public/nl/demon/shadowland/freedumbytes/maven/owasp/dependency-check/${customSuppressionHintsFolderVersion}
    </customSuppressionHintsPath>
    <customSuppressionFile>
      ${customSuppressionHintsPath}/dependency-check-${customSuppressionHintsVersion}-suppression.xml
    </customSuppressionFile>
    <customHintsFile>
      ${customSuppressionHintsPath}/dependency-check-${customSuppressionHintsVersion}-hints.xml
    </customHintsFile>
  </properties>

  <profiles>
    <profile>
      <id>enableDependencyCheckReport</id>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.owasp</groupId>
            <artifactId>dependency-check-maven</artifactId>
            <version>${mavenDependencyCheckVersion}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>aggregate</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.owasp</groupId>
          <artifactId>dependency-check-maven</artifactId>
          <version>${mavenDependencyCheckVersion}</version>
          <configuration>
            <name>Dependency Check Report</name>
            <outputDirectory>${owaspReportsPath}</outputDirectory>
            <format>${owaspReportFormat}</format>
            <cveValidForHours>${cveValidForHours}</cveValidForHours>

            <suppressionFile>${customSuppressionFile}</suppressionFile>
            <hintsFile>${customHintsFile}</hintsFile>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

##### Sample Report Showing Vulnerable Dependencies

![Dependency Check Sample Default](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-default.png "Dependency Check Sample Default")

 **Note**: Since `OWASP Dependency Check Plugin` release `3.1.0` (see also [pull request 1028](https://github.com/jeremylong/DependencyCheck/pull/1028) False Positive Reduction) these custom suppression and hints files are obsolete
 unless the `Vulnerabilities` should show up as `Suppressed`.

![Dependency Check Sample Custom](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom.png "Dependency Check Sample Custom")

##### Sample Report Showing All Dependencies

![Dependency Check Sample Custom All](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom-all.png "Dependency Check Sample Custom All")

##### Sample Report Showing Custom Suppressed Vulnerabilities

![Dependency Check Sample Custom Suppressed Collapsed](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-collapsed.png "Dependency Check Sample Custom Suppressed Collapsed")

![Dependency Check Sample Custom Suppressed Expanded](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-expanded.png "Dependency Check Sample Custom Suppressed Expanded")

#### 4. Bill Of Materials

To define the required versions [BOM](https://gitlab.com/freedumbytes/bill-of-materials) of the most popular Maven dependencies:

```xml
  <properties>
    <bomVersion>x.y.z</bomVersion>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>nl.demon.shadowland.freedumbytes.maven.dependencies</groupId>
        <artifactId>bom</artifactId>
        <version>${bomVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
```

To include Grouping Log/Test Dependencies:

```xml
  <properties>
    <bomVersion>x.y.z</bomVersion>
    <groupVersion>${bomVersion}</groupVersion>
  </properties>

  <dependencies>
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.dependencies</groupId>
      <artifactId>log</artifactId>
      <version>${groupVersion}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.dependencies</groupId>
      <artifactId>test</artifactId>
      <version>${groupVersion}</version>
      <type>pom</type>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

**Note**: Use scope test to restrict the dependencies to the test phase only.

Overview of Maven Dependencies Groupings and their content:

  * [Common](https://freedumbytes.gitlab.io/bill-of-materials/grouping/common/index.html)
  * [Spring](https://freedumbytes.gitlab.io/bill-of-materials/grouping/spring/index.html)
  * [Hibernate](https://freedumbytes.gitlab.io/bill-of-materials/grouping/hibernate/index.html)
  * [Jersey](https://freedumbytes.gitlab.io/bill-of-materials/grouping/jersey/index.html)
  * [Test](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/index.html)
  * [Log](https://freedumbytes.gitlab.io/bill-of-materials/grouping/log/index.html)
  * [MySQL](https://freedumbytes.gitlab.io/bill-of-materials/grouping/mysql/index.html)
  * [Tomcat](https://freedumbytes.gitlab.io/bill-of-materials/grouping/tomcat/index.html)


##### 4.1 [Monkey Island](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/about.html)

The term Mock Objects has become a popular one to describe special case objects that mimic real objects for testing.

Expect-run-verify is the most common pattern in mock frameworks. [What's wrong with it](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/expect-run-verify-goodbye.html)?
[Can I test what I want, please](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/can-i-test-what-i-want-please.html)? I'm a mockist but existing mock frameworks just don't appeal to me.
They spoil my TDD experience. They harm code readability. I needed something better. That's why I came up with [Mockito](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/mockito.html).

I'm about to show that it is very useful to distinguish two basic kinds of interactions: [asking an object of data and telling an object to do something](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/is-there-a-difference-between-asking-and-telling.html).
Fair enough, it may sound obvious but most mocking frameworks treat all interactions equally and don't take advantage of the distinction.

By now, you’ve probably figured out that Mockito is not strictly a mocking framework. It’s rather a spying/stubbing framework but that’s a story for a [different post](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/lets-spy.html).

  * Mockito - [The New Mock Framework on the Block](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/behind-the-times/the-new-mock-framework-on-the-block.html).
  * [Mocks and Stubs aren't Spies](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/behind-the-times/mocks-and-stubs-arent-spies.html).
  * [Mocks Aren't Stubs](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/martin-fowler/mocks-arent-stubs.html).

#### 5. Custom Maven Skins

To use for example the [Custom Reflow Skin](https://gitlab.com/freedumbytes/skins):

```xml
  <properties>
    <skinGroupId>nl.demon.shadowland.freedumbytes.maven.custom.skins</skinGroupId>
    <skinArtifactId>maven-reflow-skin</skinArtifactId>
    <skinVersion>${customMavenReflowSkinVersion}</skinVersion>

    <customSkinVersion>x.y.z</customSkinVersion>
    <customMavenPaperSkinVersion>${customSkinVersion}</customMavenPaperSkinVersion>
    <customMavenFluidoSkinVersion>${customSkinVersion}</customMavenFluidoSkinVersion>
    <customMavenReflowSkinVersion>${customSkinVersion}</customMavenReflowSkinVersion>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${mavenSitePluginVersion}</version>
          <dependencies>
            <dependency>
              <groupId>nl.demon.shadowland.freedumbytes.patch.lt.velykis.maven.skins</groupId>
              <artifactId>reflow-velocity-tools</artifactId>
              <version>${customMavenReflowSkinVersion}</version>
            </dependency>
          </dependencies>
          <configuration>
            <generateSitemap>${generateSitemap}</generateSitemap>
            <inputEncoding>${project.build.sourceEncoding}</inputEncoding>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

And the corresponding site.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/DECORATION/1.8.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/DECORATION/1.8.0 https://maven.apache.org/xsd/decoration-1.8.0.xsd"
         combine.self="override">
  <skin>
    <groupId>${skinGroupId}</groupId>
    <artifactId>${skinArtifactId}</artifactId>
    <version>${skinVersion}</version>
  </skin>

  <publishDate position="bottom" format="yyyy-MM-dd HH:mm:ss z" />
  <version position="navigation-bottom" />

  <custom>
    <reflowSkin>
      <localResources>true</localResources>

      <highlightJs>true</highlightJs>

      <topNav>parent|modules|reports</topNav>
      <navbarInverse>true</navbarInverse>

      <breadcrumbs>false</breadcrumbs>
      <toc>false</toc>

      <skinAttribution>false</skinAttribution>
    </reflowSkin>
  </custom>

  <body>
    <menu inherit="bottom" ref="parent" />
    <menu inherit="bottom" ref="modules" />
    <menu inherit="bottom" ref="reports" />
  </body>
</project>
```

#### 6. JModalWindow

  * [![JModalWindow SUN Article](https://freedumbytes.gitlab.io/setup/images/icon/jmodalwindow.png "JModalWindow SUN Article")](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) [JModalWindow SUN Article](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) about the [JModalWindow Project](https://gitlab.com/freedumbytes/jmodalwindow).
  * Options to run the SUN Article Sample and the newer complete Demo (don't forget to run `mvn compile` first):
    * `mvn exec:exec -pl sample`
    * `mvn exec:exec -pl demo [-Dextra.argument=-minimal]`

**Note**: The optional command line option `extra.argument` with value `-minimal` disables the blurring of a blocked window and the busy cursor, when moving the mouse cursor over it. It also disables iconify of a blocked internal frame.

#### 7. Basketball

  * [![Basketball](https://freedumbytes.gitlab.io/setup/images/icon/basketball.png "Basketball")](https://gitlab.com/freedumbytes/basketball/) [Basketball](https://gitlab.com/freedumbytes/basketball/) Data Storage of Game Statistics to demonstrate:
    * How to use this OpenSource project structure setup.
    * [Eclipse Jersey](https://projects.eclipse.org/projects/ee4j.jersey/) REST framework.
    * [React JavaScript](https://reactjs.org/) library for building user interfaces.

#### 8. SonarQube/SonarCloud Maven Report Plugin

Add the [SonarQube/SonarCloud Maven Report Plugin](https://github.com/SonarQubeCommunity/sonar-maven-report) to the reporting section in the POM:

```xml
<project>
  …

  <reporting>
    <plugins>
      <plugin>
        <groupId>nl.demon.shadowland.maven.plugins</groupId>
        <artifactId>sonarqube-maven-report</artifactId>
        <version>0.2.x</version>
      </plugin>
    </plugins>
  </reporting>
</project>
```

